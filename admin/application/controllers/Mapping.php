<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * User Management class created by CodexWorld
 */
class Mapping extends MY_Controller {

    function __construct() {
        $this->data = parent::__construct();

        $this->load->library('form_validation');
    }


    public function index() {

        $cityResult['cities'] = $this->Cities->cityList();
        //echo '<pre>'; print_r($cityResult); exit;
        $this->data = array_merge($this->data, $cityResult);

        $this->load->view('city/list', $this->data);
    }
    
    public function suppliers() {

        $content = array();
        $content['title'] = "Suppliers country mapping";
        $content['sub_title'] = "<small><i>Suppliers country mapping</i></small>";
        $content['success'] = "";
        $content['suppliers'] = $this->fetch_suppliers();
        $content['countries'] = $this->Cities->countryList();

        $this->data = array_merge($this->data, $content);               
        return $this->load->view('suppliermapping', $this->data);
    }
    
    public function fetch_suppliers(){
        $suppliers = array();
        $suppliers = $this->Device_model->getAllSuppliers();
        return $suppliers;
    }
    
    public function updateSupplierCountries(){
        $supplierId = $_GET['supplier_id'];
        $countries = $_GET['countries'];
        $this->Device_model->updateSuppliersCountries($supplierId,$countries);
        redirect(base_url() . "mapping/suppliers?updated=yes");
       // return $this->load->view('syncjson',$this->data);
        
    }
    
    
    public function devices() {

        $content = array();
        $content['title'] = "Devices country mapping";
        $content['sub_title'] = "<small><i>Devices country mapping</i></small>";
        $content['success'] = "";
        $content['suppliers'] = $this->fetch_devices();
        $content['countries'] = $this->Cities->countryList();

        $this->data = array_merge($this->data, $content);               
        return $this->load->view('devicemapping', $this->data);
    }
    
    public function fetch_devices(){
        $device = array();
        $device = $this->Device_model->getAllDevicesList();
        return $device;
    }
    
    public function updateDeviceCountries(){
        $supplierId = $_GET['supplier_id'];
        $countries = $_GET['countries'];
        $this->Device_model->updateDevicesCountries($supplierId,$countries);
        redirect(base_url() . "mapping/devices?updated=yes");
       // return $this->load->view('syncjson',$this->data);
        
    }

   
    
}
