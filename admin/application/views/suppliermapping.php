 
<?php 
$this->load->view('header');

?>
<script type="text/javascript">
    
$(document).ready(function() {

});

function execute(){ 
        var url = "<?php  echo base_url() ?>report/syncjsondata";
        $(location).attr('href',url);
}

function updateCountries(id,name){ 
        var updatedCountries = $('#supplierid_'+id).val();
        var url = "<?php  echo base_url() ?>mapping/updateSupplierCountries?supplier_id="+id+"&&countries="+updatedCountries;
        $(location).attr('href',url);
}
</script>
<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
<!--              <div class="title_left">
                <h3>Import</h3>
              </div>-->
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title; ?></h2>
<!--                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>-->
                    <div class="clearfix"></div>
                    
                  </div>
                    
                  <div class="x_content">
                        <?php 
                               if(isset($_GET['updated'])) { ?>
                        <div id="data-success">

                            
                  <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                                <?php 
                                    echo "<b>Countries updated successfully</b>";
                                ?>
                      
                  </div>
                            </div>
                      <br />
                            <?php } ?>
                      
                    

                    
                    <?php 
                    	$split_url = explode('/',trim($_SERVER['REQUEST_URI'],'/'));
                        $project = (isset($split_url[0]))?$split_url[0]:"imdadplus";
                    
                    ?>
                      <?php // echo '<pre>'; print_r($countries);?>
                    <table>
                        <tbody>
                            <?php foreach($suppliers as $key => $val){ ?>
                            <?php
                                $countryList = explode(",",$suppliers[$key]['countries']);
                            ?>
                            <tr style="font-style: italic; height: 100px;">
                                <td style="width:400px"> <b><?php echo $suppliers[$key]['name']; ?></b> </td>
                                <td style="width:500px;">
                                    <select name="supplierid_<?php echo $suppliers[$key]['id']; ?>" id="supplierid_<?php echo $suppliers[$key]['id']; ?>" class="select_countries" multiple name="state">
                                      <?php foreach($countries as $ckey => $cval){ 
                                        $selected = "";  
                                        if (in_array($cval['countryname'], $countryList)) {
                                          $selected = "selected";
                                        }
                                      ?>  
                                        
                                      <option <?php echo $selected;?> value="<?php echo $cval['countryname']; ?>"><?php echo $cval['countryname']; ?></option>
                                      <?php } ?>
                                    </select>
                                    <?php // echo $suppliers[$key]['countries']; ?>
                                </td>
                                <td style="width:200px"><button type="submit" class="btn btn-success" id="import-submit" onclick="updateCountries(<?php echo $suppliers[$key]['id']; ?>,'<?php echo $suppliers[$key]['countries']; ?>')">Update mapping</button></td>
              
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                  </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
            </div>
        
        <!-- /page content -->

<?php 
$this->load->view('footer');
?>