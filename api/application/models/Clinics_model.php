<?php

/**
 * Created by PhpStorm.
 * User: ben
 * Date: 3/13/18
 * Time: 9:03 AM
 */
class Clinics_model extends CI_Model {

    /**
     * Get all country data based on filter
     *
     * @param $filters
     * @return array
     * @throws Exception
     */
    public function getAll($filters = array()) {

        $sql = "SELECT  cl.Clinic_Id, cl.Clinic_Name,ct.City_Name,cr.Country_Name FROM tbl_clinics as cl join tbl_city as ct on cl.City_Id = ct.City_Id
		join tbl_country as cr on cl.Country_Id = cr.Country_Id";

        echo $sql . "<br>";

        $query = $this->db->query($sql);

        // echo $sql;die;

        return $query->result();
    }
    
    public function getClinicDensity($city_id, $country_id){
            $maploadarr = array();
            $marker1 = array();
            $marker2 = array();
            $marker3 = array();
            $maploads = $this->db->query("SELECT tbl_clinics.Clinic_Latitude as lat, tbl_city.density_point_1_lat as lat1, "
                        . "tbl_city.density_point_1_lng as lng1, tbl_city.density_point_2_lat as lat2, "
                        . "tbl_city.density_point_2_lng as lng2, tbl_city.density_point_3_lat as lat3, "
                        . "tbl_city.density_point_3_lng as lng3, "
                        . "tbl_clinics.Clinic_Longitude as lng, tbl_city.set_high_density as high, "
                        . "tbl_city.set_medium_density as medium, tbl_city.set_low_density as low "
                        . "FROM  tbl_clinics "
                        . "LEFT JOIN tbl_device_install ON tbl_device_install.Clinic_ID = tbl_clinics.Clinic_ID "
                        . "LEFT JOIN tbl_city ON tbl_city.City_Id = tbl_clinics.City_Id "
                        . "WHERE tbl_clinics.City_Id = $city_id AND "
                        . "tbl_clinics.Country_Id = $country_id "
                        . "GROUP BY tbl_clinics.Clinic_Id")->result_array(); 

            
//            $ref_lat1 = ($maploads[0]['lat1'] !='')?$maploads[0]['lat1']:0;
//            $ref_lng1 = ($maploads[0]['lng1'] !='')?$maploads[0]['lng1']:0;
//            $radius1 =  ($maploads[0]['radius1'] !='')?$maploads[0]['radius1']/1000:0;
//      
//            $ref_lat2 = ($maploads[0]['lat2'] !='')?$maploads[0]['lat2']:0;
//            $ref_lng2 = ($maploads[0]['lng2'] !='')?$maploads[0]['lng2']:0;
//            $radius2 =  ($maploads[0]['radius2'] !='')?$maploads[0]['radius2']/1000:0;
//            
//            $ref_lat3 = ($maploads[0]['lat3'] !='')?$maploads[0]['lat3']:0;
//            $ref_lng3 = ($maploads[0]['lng3'] !='')?$maploads[0]['lng3']:0;
//            $radius3 =  ($maploads[0]['radius3'] !='')?$maploads[0]['radius3']/1000:0;
            
//            $high = ($maploads[0]['high'] !='')?$maploads[0]['high']:0;
//            $medium = ($maploads[0]['medium'] !='')?$maploads[0]['medium']:0;
//            $low = ($maploads[0]['low'] !='')?$maploads[0]['low']:0;

            
//            $markers1 = $this->getClinicsInRadius($ref_lat1, $ref_lng1, $radius1, $city_id, $country_id);
//            $markers2 = $this->getClinicsInRadius($ref_lat2, $ref_lng2, $radius2, $city_id, $country_id);
//            $markers3 = $this->getClinicsInRadius($ref_lat3, $ref_lng3, $radius3, $city_id, $country_id);
//            
//            $maploadarr['marker1'] = $this->iconColor($markers1, $high, $medium, $low);
//            $maploadarr['marker2'] = $this->iconColor($markers2, $high, $medium, $low);
//            $maploadarr['marker3'] = $this->iconColor($markers3, $high, $medium, $low);        
            
//            $i=0;
//            foreach($maploads as $key => $val){
//                $maploads[$i]['icon']  = 'blue.png';
//                $maploads[$i]['type']  = 'normal';
//                $i++;
//            }
//          //echo '<pre>'; print_r($maploadarr);
//            $j = count($maploads);
//            $p = 0;
//            foreach($maploadarr as $k => $v){
//                if($p == 0){
//                    $maploadss[$j]['lat']  = $maploads[0]['lat1'];
//                    $maploadss[$j]['lng']  = $maploads[0]['lng1'];
//                    $maploadss[$j]['icon']  = $maploadarr['marker1']['icon'];
//                    $maploadss[$j]['type']  = 'density';    
//                }
//                if($p == 1){
//                    $maploadss[$j]['lat']  = $maploads[0]['lat2'];
//                    $maploadss[$j]['lng']  = $maploads[0]['lng2'];
//                    $maploadss[$j]['icon']  = $maploadarr['marker2']['icon'];
//                    $maploadss[$j]['type']  = 'density';    
//                }
//                if($p == 2){
//                    $maploadss[$j]['lat']  = $maploads[0]['lat3'];
//                    $maploadss[$j]['lng']  = $maploads[0]['lng3'];
//                    $maploadss[$j]['icon']  = $maploadarr['marker3']['icon'];
//                    $maploadss[$j]['type']  = 'density';    
//                }   
//                $p++;
//               $j++; 
//            }
//            
//            $maploads = array_merge($maploads, $maploadss);
          // echo '<pre>'; print_r($maploads); exit;
        return $maploads;
        //return $maploadarr;
        
    } 
    
    function getClinicsInRadius($ref_lat, $ref_lng, $radius, $city_id, $country_id){

                $densityarr =  $this->db->query("SELECT *
                            FROM (
                              SELECT `Clinic_Latitude`, `Clinic_Longitude`,
                                3956 * ACOS(COS(RADIANS($ref_lat)) * COS(RADIANS(Clinic_Latitude)) * COS(RADIANS($ref_lng) - RADIANS(Clinic_Longitude)) + SIN(RADIANS($ref_lat)) * SIN(RADIANS(Clinic_Latitude))) AS `distance`
                              FROM `tbl_clinics`
                              WHERE
                                `Clinic_Latitude` 
                                  BETWEEN $ref_lat - ($radius / 69) 
                                  AND $ref_lat + ($radius / 69)
                                AND `Clinic_Longitude` 
                                  BETWEEN $ref_lng - ($radius / (69 * COS(RADIANS($ref_lat)))) 
                                  AND $ref_lng + ($radius / (69* COS(RADIANS($ref_lat))))
                                AND `City_Id` = $city_id 
                                AND `Country_Id` = $country_id
                            ) r
                            WHERE `distance` < $radius "
                . "ORDER BY distance ASC")->result_array(); 
                
                return $densityarr;
    }
    
    function iconColor($markers, $highcount, $mediumcount, $lowcount){
        
            $high = array('icon' => 'high_density.png');
            $medium = array('icon' => 'medium_density.png');
            $low = array('icon' => 'low_density.png');
        
            if(count($markers) >= $highcount){
                //$markers = array_merge($high, $markers);
                $markers = $high;
            } else if(count($markers) < $highcount && count($markers) >= $mediumcount){
                $markers = $medium;
            }  else if(count($markers) < $mediumcount && count($markers) >= $lowcount){
                $markers = $low;
            }
            
            return $markers;
        
    }
    public function getCityDensity($country_id){
        $cities = $this->db->query("SELECT DISTINCT c.City_Id as id, c.Nearest_city_id as Nearest_city_id,c.Country_Id as Country_Id,c.City_Latitude as lat, c.City_Longitude as lng, c.City_Name as name, c.sitecore_zoom as zoom, c.sitecore_lat as center_lat,
        c.sitecore_lng as center_lng, c.marker_type as marker_type FROM tbl_city AS c where c.Country_Id = $country_id")->result_array();
        
    
        return $cities;
    }
}
