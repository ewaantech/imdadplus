var zoomedIn = false;
function smoothZoomV2 (map, level, cnt, mode) {
    //alert('Count: ' + cnt + 'and Max: ' + level);

    // If mode is zoom in
    if(mode == true) {

        if (cnt >= level) {
            return;
        }
        else {
            var z = google.maps.event.addListener(map, 'zoom_changed', function(event){
                google.maps.event.removeListener(z);
                smoothZoomV2(map, level, cnt + 1, true);
            });
            setTimeout(function(){map.setZoom(cnt)}, 80);
        }
    } else {
        if (cnt < level) {
            return;
        }
        else {
            console.log("Zoom Out : ", cnt);
            var z = google.maps.event.addListener(map, 'zoom_changed', function(event) {
                google.maps.event.removeListener(z);
                smoothZoomV2(map, level, cnt - 1, false);
            });
            setTimeout(function(){map.setZoom(cnt)}, 200);
        }
    }
} 