<?php

define("MODE","PRODUCTION");  // ["LOCAL", "STAGING", "PRODUCTION"]

switch( strtoupper(MODE) ){
    default:
    case "LOCAL":
        define("SITE_URL","http://localhost/imdadplus");
        break;
    case "STAGING":
        define("SITE_URL","http://ewaantech.com/imdadplus");
        break;
    case "PRODUCTION":
        // define("SITE_URL","http://imdadplus.com");
        define("SITE_URL","http://localhost/imdadplus");
        break;
}
