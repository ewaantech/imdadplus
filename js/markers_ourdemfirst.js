var infoWindow = null;
var clinictype = null;
var clinicslug = null;
var defaultJSON = {"installation_base": 0, "avg_repair_time": 0, "repair_time_24h": 0, "calls_solved_12h": 0, "fix_rate": 0, "spare_parts_hub": 0, "doctors_trained": "0", "nurses_trained": "0", "elearning_courses": 0, "campaigns_running": 0, "microsite_visitors": "0", "clinic_locator_actions": 0, "patient_leads": 0, "device_name": "", "device_id": 0, "clients": 0, "solutions": "0", "market_share": 0, "avg_repair_time_12h_warranty": 0, "total_repair_calls_warranty": "0", "avg_repair_time_12h_all": 0, "total_repair_calls_all": "0", "first_time_fix_rate": 0, "ftfr_total_repair_calls_all": "0", "up_time": "0", "e_learning_course_delivered": "0", "locator_actions": "0", "qualified_leads": "0", "gentle_laster": "-", "ultraformer": "-", "spectra": "-", "enCurve": "-", "Infini": "-", "eCo2": "-", "ActionII": "-", "artas": "-", "clarity": "-","facebook_likes":"0","instagram_likes":"0"}
// the smooth zoom function
function smoothZoom(map, max, cnt) {


    if (cnt > max) {
        return;
    } else {
        z = google.maps.event.addListener(map, 'zoom_changed', function (event) {
            google.maps.event.removeListener(z);
            smoothZoom(map, max, cnt + 1);
        });
        setTimeout(function () {
            map.setZoom(cnt)
        }, 80);
    }
}





function smoothZoomOut(map, max, cnt) {
    if (cnt < max) {
        return;
    } else {
        z = google.maps.event.addListener(map, 'zoom_changed', function (event) {
            google.maps.event.removeListener(z);
            smoothZoom(map, max, cnt - 1);
        });
        setTimeout(function () {
            map.setZoom(cnt)
        }, 60); // 80ms is what I found to work well on my system -- it might not work well on all systems
    }
}



function fit_bounds_markers_out(zoom_level) {
    smoothZoomV2(map, zoom_level, map.getZoom(), false);
}


function fit_bounds_markers(zoom_level) {


    //(optional) restore the zoom level after the map is done scaling
    var listener = google.maps.event.addListener(map, "idle", function () {
        google.maps.event.removeListener(listener);
    });
    smoothZoom(map, zoom_level, map.getZoom());
    //render_markers();

}

function set_center(address) {
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({address: address}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
        }
    })
}


/**
 * Function to load all markers for country or city from the json file
 * @param string type
 * @param string  slug
 */
function load_previous_markers(type, slug, update_breadcrumb) {
    delete_markers();
    

    switch (type) {
        default:
            show_sections_not_for_clinics();
            // console.log("Initialize");
            bounds = new google.maps.LatLngBounds();

            jQuery.each(json.markers, function (i, item) {
                add_location_marker(item);
            });

            // console.log("GCC MARKERS",tempmarkers);
            fit_bounds_markers(6);

            // map.fitBounds(bounds);

            // now populate the markers in the zoomed map
            setTimeout(function () {
                render_markers();
            }, 500);

            //console.log( eval("json.countries['kingdom-of-saudi-arabia'].label") );
            set_center("Kingdom of Saudi Arabia"); // ksa center;
            // resets the te    mporary markers so we can use it again
            //reset_temp_markers();
            FloatingPopup.populate_popup(eval("json.report.alldevice"));

        case 'city':
            show_sections_not_for_clinics();
            bounds = new google.maps.LatLngBounds();


            //console.log("Countries zoom :", eval("json.countries."+slug+".zoom" ));
            fit_bounds_markers_out(eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".zoom"));
            console.log("json.clinics." + breadcrumb[1] + ".cities." + slug);
            $.each(eval("json.clinics." + breadcrumb[1] + "." + slug), function (i, item) {
                //console.log("Country", item.markers);
                add_location_marker(item);

            });

            // load service hubs, asked to hide this for country 1620102
            //load_service_hubs(slug,null);




            // now populate the markers in the zoomed map
            setTimeout(function () {
                render_markers();
            }, 500);

            //console.log( eval("json.countries['kingdom-of-saudi-arabia'].label") );
            //set_center(  eval("json.countries."+slug+".label" ) ); // ksa center;
            // resets the te    mporary markers so we can use it again
            //reset_temp_markers();

            set_center(
                    eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".center_lat") + "," +
                    eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".center_lng")
                    );


            FloatingPopup.populate_popup(eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".report"));

            jQuery("#mapview span.mapview-label").html(
                    eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".label") + ", " +
                    eval("json.countries." + breadcrumb[1] + ".label"));
            jQuery("#mapview span.mapviewshow-label").html(
                    eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".label") + " <span class=\"seperator\"></span> " +
                    eval("json.countries." + breadcrumb[1] + ".label"));
            jQuery("#city_count").html("<span class='seperator'></span> Cities : " + eval("json.countries." + breadcrumb[1] + ".city_count"));
            
            var str1 = $('.mapview-label').text();

            if (str1 == "Middle East") {
                $('[id^="maintab_"]').show();
                $('[id^="device_"]').show();
                if ($('.tab-pane:first').is(':visible') == false ) {
                    $('[id^="maintab_"]:first > a').click();
                }
            } else {
                var pieces = str1.split(/[\s,]+/);
                var countryName = $.trim(pieces[pieces.length-1]);
                var ajax_url = "/api/report/deviceByCountry?country=" + countryName;
                $('[id^="maintab_"]').hide();
                $('[id^="device_"]').hide();
                $('[class^="tabpane_"]').hide();
                var existingchecks = [];
                $('[id^="device_"]').find(".form-check-input:checked").each(function () {
                    //alert("Id: " + $(this).attr("id") + " Value: " + $(this).val());
                    existingchecks.push($(this).attr("id"));
                });
                $('[id^="device_"]').find(".form-check-input"). prop("checked", false);
                jQuery.get(ajax_url, function (response) {
                    resjson = response;
                    $.each(resjson, function( index, value ) {
                        $("#maintab_" + slugify(value.label)).show();
                        if (index == '0') {
                            if ($("#maintab_"+slugify(value.label)+" > a.active").length < 1) {
                                $("#maintab_"+slugify(value.label)+" > a").click();
                            }
                            firsttab = slugify(value.label);
                            $.each(value.devices, function( dev_index, dev_value ) {
                              $("#device_" + slugify(dev_value.label)).show();
                            });
                            // $(".tabpane_" + slugify(value.label)).show();
                        }
                        $.each(value.devices, function( dev_index, dev_value ) {
                          if (jQuery.inArray( slugify(dev_value.label), existingchecks ) != -1) {
                                $("#"+slugify(dev_value.label)).prop("checked", true);                         
                          }
                        });
                    });
                });
            }
    
//            var str2 = "Kuwait";
//            if(str1.indexOf(str2) !== -1) {
//                // For device filter area  
//                $("#maintab_candela").hide();
//                $("#candela").hide();
//                $("#device_16-picoplus").show();
//                $("#device_06-clarity").show();
//                $("#candela").find(".form-check-input"). prop("checked", false);
//                if ($('.tab-pane').is(':visible') == false ) {
//                   $("#maintab_lutronics > a").click(); 
//                }
//            } else {
//                // For device filter area
//                $("#maintab_candela").show();
//                $("#device_16-picoplus").hide();
//                $("#device_06-clarity").hide();
//                $("#16-picoplus"). prop("checked", false);
//                $("#06-clarity"). prop("checked", false);
//            }
            filterDeviceCall();
            break;

        case 'country':
            show_sections_not_for_clinics();
            bounds = new google.maps.LatLngBounds();


            fit_bounds_markers_out(eval("json.countries." + slug + ".zoom"));

            var bothmarkers = [];         
            $.each(eval("json.countries." + slug + ".cities"), function (i, item) {

                if (item.markers.marker_type == 'both') {
                    bothmarkers.push(item);
                } else {
                    add_location_marker(item.markers);
                }
            });

            $.each(bothmarkers, function (i, item) {
                add_location_marker(item.markers);
            });


            // now populate the markers in the zoomed map
            setTimeout(function () {
                render_markers();
            }, 500);

            set_center(eval("json.countries." + slug + ".center_lat") + "," + eval("json.countries." + slug + ".center_lng"));


            FloatingPopup.populate_popup(eval("json.countries." + slug + ".report"));
            jQuery("#mapview span.mapview-label").html(eval("json.countries." + slug + ".label"));
            jQuery("#mapview span.mapviewshow-label").html(eval("json.countries." + slug + ".label"));
            jQuery("#city_count").html("<span class='seperator'></span> Cities : " + eval("json.countries." + slug + ".city_count"));
            
            var str1 = $('.mapview-label').text();

            if (str1 == "Middle East") {
                $('[id^="maintab_"]').show();
                $('[id^="device_"]').show();
                if ($('.tab-pane:first').is(':visible') == false ) {
                    $('[id^="maintab_"]:first > a').click();
                }
            } else {
                var pieces = str1.split(/[\s,]+/);
                var countryName = $.trim(pieces[pieces.length-1]);
                var ajax_url = "/api/report/deviceByCountry?country=" + countryName;
                $('[id^="maintab_"]').hide();
                $('[id^="device_"]').hide();
                $('[class^="tabpane_"]').hide();
                var existingchecks = [];
                $('[id^="device_"]').find(".form-check-input:checked").each(function () {
                    //alert("Id: " + $(this).attr("id") + " Value: " + $(this).val());
                    existingchecks.push($(this).attr("id"));
                });
                $('[id^="device_"]').find(".form-check-input"). prop("checked", false);
                jQuery.get(ajax_url, function (response) {
                    resjson = response;
                    $.each(resjson, function( index, value ) {
                        $("#maintab_" + slugify(value.label)).show();
                        if (index == '0') {
                            if ($("#maintab_"+slugify(value.label)+" > a.active").length < 1) {
                                $("#maintab_"+slugify(value.label)+" > a").click();
                            }
                            firsttab = slugify(value.label);
                            $.each(value.devices, function( dev_index, dev_value ) {
                              $("#device_" + slugify(dev_value.label)).show();
                            });
                            // $(".tabpane_" + slugify(value.label)).show();
                        }
                        $.each(value.devices, function( dev_index, dev_value ) {
                          if (jQuery.inArray( slugify(dev_value.label), existingchecks ) != -1) {
                                $("#"+slugify(dev_value.label)).prop("checked", true);                         
                          }
                        });
                    });
                });
            }
    
//            var str2 = "Kuwait";
//            if(str1.indexOf(str2) !== -1) {
//                // For device filter area  
//                $("#maintab_candela").hide();
//                $("#candela").hide();
//                $("#device_16-picoplus").show();
//                $("#device_06-clarity").show();
//                $("#candela").find(".form-check-input"). prop("checked", false);
//                if ($('.tab-pane').is(':visible') == false ) {
//                   $("#maintab_lutronics > a").click(); 
//                }
//            } else {
//                // For device filter area
//                $("#maintab_candela").show();
//                $("#device_16-picoplus").hide();
//                $("#device_06-clarity").hide();
//                $("#16-picoplus"). prop("checked", false);
//                $("#06-clarity"). prop("checked", false);
//            }
            filterDeviceCall();
            break;
        case 'gcc':
            show_sections_not_for_clinics();
            bounds = new google.maps.LatLngBounds();
            $.each(json.markers, function (i, item) {
                add_location_marker(item);
            });



            fit_bounds_markers_out(6);

            // now populate the markers in the zoomed map
            setTimeout(function () {
                render_markers();
            }, 500);

            FloatingPopup.populate_popup(eval("json.report"));
            jQuery("#mapview span.mapview-label").html("Middle East");
            jQuery("#mapview span.mapviewshow-label").html("Middle East");
            jQuery("#city_count").html("<span class='seperator'></span> Cities : " + eval("json.countries." + slug + ".city_count"));
            // For device filter area
            $('[id^="maintab_"]').show();
            $('[id^="device_"]').show();
            if ($('.tab-pane:first').is(':visible') == false ) {
                $('[id^="maintab_"]:first > a').click();
            }
//            $("#maintab_candela").show();
//            if($("#candela").is(":hidden")) {
//                $("#maintab_candela > a").click();
//                $("#candela").show();
//            }
//            $("#device_16-picoplus").hide();
//            $("#device_06-clarity").hide();
//            $("#16-picoplus"). prop("checked", false);
//            $("#06-clarity"). prop("checked", false);
            filterDeviceCall();
            break;

    }
}


function reload_map() {
    if (breadcrumb.length > 4) {
        var changed_country_name = breadcrumb[1];
        var changed_city_slug = breadcrumb[2];
        var changed_clinic = breadcrumb[breadcrumb.length - 1];
        
        breadcrumb = ["gcc"];
        breadcrumb.push(changed_country_name);
        breadcrumb.push(changed_city_slug);
        breadcrumb.push(changed_clinic);        
    }

    switch (breadcrumb.length) {
        case 4:
            clinicslug = breadcrumb[3];
            cityslug = breadcrumb[2];
            breadcrumb.pop();
            load_markers('city', cityslug, clinicslug);
            break;
        case 3:
            cityslug = breadcrumb[2];
            breadcrumb.pop();
            load_markers('city', cityslug);
            break;
        case 2:
            countryslug = breadcrumb[1];
            breadcrumb.pop();
            load_markers('country', countryslug);
            break;

        default:
            breadcrumb = ["gcc"];
            load_markers('gcc', "gcc");
    }
}



/**
 * Function to load all markers for country or city from the json file
 * @param string type
 * @param string  slug
 */
function load_markers(type, slug, clickedclinic = '') {
    delete_markers();

    var updatebc = true;

    switch (type) {
        default:

            bounds = new google.maps.LatLngBounds();

            jQuery.each(json.markers, function (i, item) {
                add_location_marker(item);
            });

            fit_bounds_markers(6);

            // now populate the markers in the zoomed map
            setTimeout(function () {
                render_markers();
            }, 500);
            set_center("Kingdom of Saudi Arabia"); // ksa center;
            FloatingPopup.populate_popup(eval("json.report.alldevice"));
            jQuery("#city_count").html("<span class='seperator'></span> Cities : " + eval("json.city_count"));
            break;
        case 'country':
            bounds = new google.maps.LatLngBounds();
            breadcrumb.push(slug);

            try {
                if ("undefined" != typeof eval("json.countries." + slug + ".report")) {
                    var bothmarkers = [];

                    $.each(eval("json.countries." + slug + ".cities"), function (i, item) {
                        
                        if (item.markers.marker_type == 'both') {
                            bothmarkers.push(item);
                        } else {
                            add_location_marker(item.markers);
                        }
                    });
                    
                    $.each(bothmarkers, function (i, item) {
                        add_location_marker(item.markers);
                    });

                    fit_bounds_markers(eval("json.countries." + slug + ".zoom"));

                    FloatingPopup.populate_popup(eval("json.countries." + slug + ".report"));

                    set_center(eval("json.countries." + slug + ".center_lat") + "," + eval("json.countries." + slug + ".center_lng"));

                    jQuery("#mapview span.mapview-label").html(eval("json.countries." + slug + ".label"));
                    jQuery("#mapview span.mapviewshow-label").html(eval("json.countries." + slug + ".label"));
                    jQuery("#city_count").html("<span class='seperator'></span> Cities : " + eval("json.countries." + slug + ".city_count"));
                    
                    var str1 = $('.mapview-label').text();

                    if (str1 == "Middle East") {
                        $('[id^="maintab_"]').show();
                        $('[id^="device_"]').show();
                        if ($('.tab-pane:first').is(':visible') == false ) {
                            $('[id^="maintab_"]:first > a').click();
                        }
                    } else {
                        var pieces = str1.split(/[\s,]+/);
                        var countryName = $.trim(pieces[pieces.length-1]);
                        var ajax_url = "/api/report/deviceByCountry?country=" + countryName;
                        $('[id^="maintab_"]').hide();
                        $('[id^="device_"]').hide();
                        $('[class^="tabpane_"]').hide();
                        var existingchecks = [];
                        $('[id^="device_"]').find(".form-check-input:checked").each(function () {
                            //alert("Id: " + $(this).attr("id") + " Value: " + $(this).val());
                            existingchecks.push($(this).attr("id"));
                        });
                        $('[id^="device_"]').find(".form-check-input"). prop("checked", false);
                        jQuery.get(ajax_url, function (response) {
                            resjson = response;
                            $.each(resjson, function( index, value ) {
                                $("#maintab_" + slugify(value.label)).show();
                                if (index == '0') {
                                    if ($("#maintab_"+slugify(value.label)+" > a.active").length < 1) {
                                        $("#maintab_"+slugify(value.label)+" > a").click();
                                    }
                                    firsttab = slugify(value.label);
                                    $.each(value.devices, function( dev_index, dev_value ) {
                                      $("#device_" + slugify(dev_value.label)).show();
                                    });
                                    // $(".tabpane_" + slugify(value.label)).show();
                                }
                                $.each(value.devices, function( dev_index, dev_value ) {
                                  if (jQuery.inArray( slugify(dev_value.label), existingchecks ) != -1) {
                                        $("#"+slugify(dev_value.label)).prop("checked", true);                         
                                  }
                                });
                            });
                        });
                    }
                    
//                    var str2 = "Kuwait";
//                    if(str1.indexOf(str2) !== -1) {
//                        // For device filter area  
//                        $("#maintab_candela").hide();
//                        $("#candela").hide();
//                        $("#device_16-picoplus").show();
//                        $("#device_06-clarity").show();
//                        $("#candela").find(".form-check-input"). prop("checked", false);
//                        if ($('.tab-pane').is(':visible') == false ) {
//                           $("#maintab_lutronics > a").click(); 
//                        }
//                    } else {
//                        // For device filter area
//                        $("#maintab_candela").show();
//                        $("#device_16-picoplus").hide();
//                        $("#device_06-clarity").hide();
//                        $("#16-picoplus"). prop("checked", false);
//                        $("#06-clarity"). prop("checked", false);
//                    }
                    filterDeviceCall();

                } else {
                    FloatingPopup.populate_popup(defaultJSON);
                }
            } catch (e) {
                FloatingPopup.populate_popup(defaultJSON);
            }





            // now populate the markers in the zoomed map
            setTimeout(function () {
                render_markers();
            }, 500);

            break;
        case 'city':
            
            if (clickedclinic != '') {
                breadcrumb.push(clickedclinic);
                try {
                    if ("undefined" != typeof eval("json.countries." + breadcrumb[1] + ".cities." + breadcrumb[2] + ".clinics." + clickedclinic + ".report")) {
                        FloatingPopup.populate_popup(eval("json.countries." + breadcrumb[1] + ".cities." + breadcrumb[2] + ".clinics." + clickedclinic + ".report"));
                    } else {
                        FloatingPopup.populate_popup(defaultJSON);
                    }
                } catch (e) {
                    FloatingPopup.populate_popup(defaultJSON);
                }


                // loading clinics for city
                load_clinics(breadcrumb[1], breadcrumb[2], null);


                try {
                    if ("undefined" != typeof eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".report")) {
                        // bound centered area
                        fit_bounds_markers(eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".zoom"));
                        set_center(eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".center_lat") + ", " + eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".center_lng")); // + "," +eval("json.countries."+breadcrumb[1]+".label")); // ksa center;
                    } else {
                        FloatingPopup.populate_popup(defaultJSON);
                    }
                } catch (e) {
                    FloatingPopup.populate_popup(defaultJSON);
                }
            } else {
                breadcrumb.push(slug);


                try {
                    if ("undefined" != typeof eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".report")) {
                        FloatingPopup.populate_popup(eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".report"));
                    } else {
                        FloatingPopup.populate_popup(defaultJSON);
                    }
                } catch (e) {
                    FloatingPopup.populate_popup(defaultJSON);
                }

                // loading clinics for city
                load_clinics(breadcrumb[1], slug, null);


                try {
                    if ("undefined" != typeof eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".report")) {
                        // bound centered area
                        fit_bounds_markers(eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".zoom"));
                        set_center(eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".center_lat") + ", " + eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".center_lng")); // + "," +eval("json.countries."+breadcrumb[1]+".label")); // ksa center;
                    } else {
                        FloatingPopup.populate_popup(defaultJSON);
                    }
                } catch (e) {
                    FloatingPopup.populate_popup(defaultJSON);
                }
            }

            // now populate the markers in the zoomed map
            setTimeout(function () {
                if (clickedclinic != '') {
                    render_markers_clicked_clinic(clickedclinic);
                } else {
                   render_markers(); 
                }
            }, 500);

            
            if (clickedclinic != '') {
                try {
                    if ("undefined" != typeof eval("json.countries." + breadcrumb[1] + ".cities." + breadcrumb[2] + ".clinics." + clickedclinic  + ".label")) {
                        jQuery("#mapview span.mapview-label").html(
                                eval("json.countries." + breadcrumb[1] + ".cities." + breadcrumb[2] + ".clinics." + clickedclinic + ".label") + ", <br />" +
                                eval("json.countries." + breadcrumb[1] + ".cities." + breadcrumb[2] + ".label") + ", " +
                                eval("json.countries." + breadcrumb[1] + ".label")
                                );
                                var clname = eval("json.countries." + breadcrumb[1] + ".cities." + breadcrumb[2] + ".clinics." + clickedclinic + ".label");
                                var clinicmodified = clname.split("|");
                                var modifiedname = '';
                                var i;
                                if (clinicmodified.length == 1) {
                                  modifiedname = clinicmodified[0];  
                                } else {
                                    for(i=0; i<clinicmodified.length-1; i++)
                                    {
                                        modifiedname = modifiedname+""+clinicmodified[i];
                                    }
                                }
                        jQuery("#mapview span.mapviewshow-label").html(
                                modifiedname + " <span class=\"seperator\"></span>" +
                                eval("json.countries." + breadcrumb[1] + ".cities." + breadcrumb[2] + ".label") + " <span class=\"seperator\"></span> " +
                                eval("json.countries." + breadcrumb[1] + ".label")
                                );
                    }
                } catch (e) {
                    console.log("Data not found");
                }
                jQuery("#city_count").html("");
            } else {

                try {
                    if ("undefined" != typeof eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".label")) {
                        jQuery("#mapview span.mapview-label").html(eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".label") + ", " + eval("json.countries." + breadcrumb[1] + ".label"));
                        jQuery("#mapview span.mapviewshow-label").html(eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".label") + " <span class=\"seperator\"></span> " + eval("json.countries." + breadcrumb[1] + ".label"));
                    }
                } catch (e) {

                }
                jQuery("#city_count").html("");
            }
            break;
        case 'clinic':
            console.log("Map laoding clinic");
            breadcrumb.push(slug);
            //console.log("Breadcrumbs - City: ",breadcrumb);

            console.log("json.countries." + breadcrumb[1] + ".cities." + breadcrumb[2] + ".clinics." + slug + ".report");
            try {
                if ("undefined" != typeof eval("json.countries." + breadcrumb[1] + ".cities." + breadcrumb[2] + ".clinics." + slug + ".report")) {
                    FloatingPopup.populate_popup(eval("json.countries." + breadcrumb[1] + ".cities." + breadcrumb[2] + ".clinics." + slug + ".report"));
                } else {
                    FloatingPopup.populate_popup(defaultJSON);
                }
            } catch (e) {
                FloatingPopup.populate_popup(defaultJSON);
            }

            // loading clinics for city
            console.log('Load marker clinic', slug);
            load_clinics(breadcrumb[1], breadcrumb[2], slug);

            try {
                if ("undefined" != eval("json.countries." + breadcrumb[1] + ".cities." + breadcrumb[2] + ".clinics." + slug + ".zoom")) {
                    fit_bounds_markers(eval("json.countries." + breadcrumb[1] + ".cities." + breadcrumb[2] + ".clinics." + slug + ".zoom"));
                    set_center(eval("json.countries." + breadcrumb[1] + ".cities." + breadcrumb[2] + ".clinics." + slug + ".center_lat") + ", " + eval("json.countries." + breadcrumb[1] + ".cities." + breadcrumb[2] + ".clinics." + slug + ".center_lng")); // + "," +eval("json.countries."+breadcrumb[1]+".label")); // ksa center;
                }
            } catch (e) {
                console.log(e.toString());
            }

            // now populate the markers in the zoomed map
            setTimeout(function () {
                render_markers();
            }, 500);

            try {
                if ("undefined" != typeof eval("json.countries." + breadcrumb[1] + ".cities." + breadcrumb[2] + ".clinics." + slug + ".label")) {
                    jQuery("#mapview span.mapview-label").html(
                            eval("json.countries." + breadcrumb[1] + ".cities." + breadcrumb[2] + ".clinics." + slug + ".label") + ", <br />" +
                            eval("json.countries." + breadcrumb[1] + ".cities." + breadcrumb[2] + ".label") + ", " +
                            eval("json.countries." + breadcrumb[1] + ".label")
                            );
                            var clname = eval("json.countries." + breadcrumb[1] + ".cities." + breadcrumb[2] + ".clinics." + slug + ".label");
                            var clinicmodified = clname.split("|");
                            var modifiedname = '';
                            var i;
                            if (clinicmodified.length == 1) {
                              modifiedname = clinicmodified[0];  
                            } else {
                                for(i=0; i<clinicmodified.length-1; i++)
                                {
                                    modifiedname = modifiedname+""+clinicmodified[i];
                                }
                            }
                    jQuery("#mapview span.mapviewshow-label").html(
                            modifiedname + " <span class=\"seperator\"></span>" +
                            eval("json.countries." + breadcrumb[1] + ".cities." + breadcrumb[2] + ".label") + " <span class=\"seperator\"></span> " +
                            eval("json.countries." + breadcrumb[1] + ".label")
                            );
                }
            } catch (e) {
                console.log("Data not found");
            }
            jQuery("#city_count").html("");
            break;
    }
}


/**
 * Function to load all markers for country or city from the json file
 * @param string type
 * @param string  slug
 */
function load_markers_indirect(type, country_name, slug, clickedclinic = '') {
    delete_markers();

    var updatebc = true;

    switch (type) {

           case 'city':
            
            if (clickedclinic != '') {
                    breadcrumb = ["gcc"];
                    breadcrumb.push(country_name);
                    breadcrumb.push(slug);
                    breadcrumb.push(clickedclinic);

                try {
                    if ("undefined" != typeof eval("json.countries." + breadcrumb[1] + ".cities." + breadcrumb[2] + ".clinics." + clickedclinic + ".report")) {
                        FloatingPopup.populate_popup(eval("json.countries." + breadcrumb[1] + ".cities." + breadcrumb[2] + ".clinics." + clickedclinic + ".report"));
                    } else {
                        FloatingPopup.populate_popup(defaultJSON);
                    }
                } catch (e) {
                    FloatingPopup.populate_popup(defaultJSON);
                }

                // loading clinics for city
                load_clinics(breadcrumb[1], slug, null);


                try {
                    if ("undefined" != typeof eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".report")) {
                        // bound centered area
                        fit_bounds_markers(eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".zoom"));
                        set_center(eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".center_lat") + ", " + eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".center_lng"));
                    } else {
                        FloatingPopup.populate_popup(defaultJSON);
                    }
                } catch (e) {
                    FloatingPopup.populate_popup(defaultJSON);
                }
            } else {
                breadcrumb.push(slug);


                try {
                    if ("undefined" != typeof eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".report")) {
                        FloatingPopup.populate_popup(eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".report"));
                    } else {
                        FloatingPopup.populate_popup(defaultJSON);
                    }
                } catch (e) {
                    FloatingPopup.populate_popup(defaultJSON);
                }

                // loading clinics for city
                load_clinics(breadcrumb[1], slug, null);


                try {
                    if ("undefined" != typeof eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".report")) {
                        // bound centered area
                        fit_bounds_markers(eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".zoom"));
                        set_center(eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".center_lat") + ", " + eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".center_lng")); // + "," +eval("json.countries."+breadcrumb[1]+".label")); // ksa center;
                    } else {
                        FloatingPopup.populate_popup(defaultJSON);
                    }
                } catch (e) {
                    FloatingPopup.populate_popup(defaultJSON);
                }
            }
            // now populate the markers in the zoomed map
            setTimeout(function () {
                if (clickedclinic != '') {
                    render_markers_clicked_clinic(clickedclinic);
                } else {
                   render_markers(); 
                }
            }, 500);

           
            if (clickedclinic != '') {
                try {
                    if ("undefined" != typeof eval("json.countries." + breadcrumb[1] + ".cities." + breadcrumb[2] + ".clinics." + clickedclinic  + ".label")) {
                        clinictype = 'city';
                        clinicslug = slug;
                        jQuery("#mapview span.mapview-label").html(
                                eval("json.countries." + breadcrumb[1] + ".cities." + breadcrumb[2] + ".clinics." + clickedclinic + ".label") + ", <br />" +
                                eval("json.countries." + breadcrumb[1] + ".cities." + breadcrumb[2] + ".label") + ", " +
                                eval("json.countries." + breadcrumb[1] + ".label")
                                );
                            var clname = eval("json.countries." + breadcrumb[1] + ".cities." + breadcrumb[2] + ".clinics." + clickedclinic + ".label");
                            var clinicmodified = clname.split("|");
                            var modifiedname = '';
                            var i;
                            if (clinicmodified.length == 1) {
                              modifiedname = clinicmodified[0];  
                            } else {
                                for(i=0; i<clinicmodified.length-1; i++)
                                {
                                    modifiedname = modifiedname+""+clinicmodified[i];
                                }
                            }
                        jQuery("#mapview span.mapviewshow-label").html(
                                modifiedname + " <span class=\"seperator\"></span>" +
                                eval("json.countries." + breadcrumb[1] + ".cities." + breadcrumb[2] + ".label") + " <span class=\"seperator\"></span> " +
                                eval("json.countries." + breadcrumb[1] + ".label")
                                );
                    }
                } catch (e) {
                    console.log("Data not found");
                }
                jQuery("#city_count").html("");
                $('#goback').show();
            } else {

                try {
                    if ("undefined" != typeof eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".label")) {
                        jQuery("#mapview span.mapview-label").html(eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".label") + ", " + eval("json.countries." + breadcrumb[1] + ".label"));
                        jQuery("#mapview span.mapviewshow-label").html(eval("json.countries." + breadcrumb[1] + ".cities." + slug + ".label") + " <span class=\"seperator\"></span> " + eval("json.countries." + breadcrumb[1] + ".label"));
                    }
                } catch (e) {

                }
                jQuery("#city_count").html("");
                $('#goback').show();
            }
            break;
    }
}

function load_service_hubs(country, city) {
    closeInfoWindow();
    if (country != null && country != "undefined" && country != "") {
        if (city != null && city != "undefined" && city != "") {
            $.each(eval("json.service_hubs." + country + "." + city), function (i, item) {
                add_location_marker(item);
            });

        } else {
            $.each(eval("json.service_hubs." + country), function (i, item) {
                $.each(item, function (i, childitem) {
                    add_location_marker(childitem);
                });
            });
        }
    }
}



function load_clinics(country, city, clinic) {
    closeInfoWindow();
    if (country != null && country != "undefined" && country != "") {
        if (city != null && city != "undefined" && city != "") {
            if (clinic != null && clinic != "undefined" && clinic != "") {
                try {
                    if ("undefined" != typeof eval("json.countries." + country + ".cities." + city + ".clinics." + clinic + ".markers")) {
                        add_location_marker(eval("json.countries." + country + ".cities." + city + ".clinics." + clinic + ".markers"));
                    }
                } catch (e) {
                    console.log("Markers not found");
                }
            } else {
                // city
                $.each(eval("json.clinics." + country + "." + city), function (i, item) {
                    add_location_marker(item);
                });
            }
        }
    }
}

function closeInfoWindow() {
    if (infoWindow !== null) {
        google.maps.event.clearInstanceListeners(infoWindow);  // just in case handlers continue to stick around
        infoWindow.close();
        infoWindow = null;
    }
}

function add_location_marker(marker) {
    tempmarkers.push(marker);
    set_map_bounds(marker);
}

function set_map_bounds(marker) {
    bounds = new google.maps.LatLngBounds();
    var pos = new google.maps.LatLng(marker.lat, marker.lng);
    bounds.extend(pos);
}

function reset_temp_markers() {
    tempmarkers = [];
}

function render_markers_clicked_clinic(clickedclinic) {
    closeInfoWindow();
    for (i = 0; i < tempmarkers.length; i++) {
        marker = tempmarkers[i];


        var pos = new google.maps.LatLng(marker.lat, marker.lng);
        var content = '<div style="max-width: 240px;text-align: left;padding: 10px 0px 10px 0px"><h4 style="font-size:14px!important;margin:0px;line-height:20px">' + marker.label + '</h4></div>';


        var icon;
        var iconcolor = ''; 
        if (marker.type == "clinic" || marker.type == "clinic_single") {
            if (marker.slug == clickedclinic) {
                icon = {
                    url: iconBase + 'orange.png', // url
                    scaledSize: new google.maps.Size(28, 28), // scaled size
                    origin: new google.maps.Point(0, 0), // origin
                    anchor: new google.maps.Point(28, 28) // anchor
                };  
                iconcolor = 'orange';
            } else {
                icon = {
                    url: iconBase + 'blue.png', // url
                    scaledSize: new google.maps.Size(28, 28), // scaled size
                    origin: new google.maps.Point(0, 0), // origin
                    anchor: new google.maps.Point(28, 28) // anchor
                };                
            }
        } else if (marker.marker_type == "service_hub") {
            icon = {
                url: iconBase + 'service_hub.png', // url
                scaledSize: new google.maps.Size(54, 54), // scaled size
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(28, 28) // anchor
            };

        } else if (marker.marker_type == "office") {
            icon = {
                url: iconBase + 'office.png', // url
                scaledSize: new google.maps.Size(54, 54), // scaled size
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(28, 28) // anchor
            };

        } else if (marker.marker_type == "both") {
            icon = {
                url: iconBase + 'both.png', // url
                scaledSize: new google.maps.Size(54, 54), // scaled size
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(28, 28) // anchor
            };

        } else {
            icon = {
                url: icons[marker.type].icon, // url
                scaledSize: new google.maps.Size(54, 54), // scaled size
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(28, 28) // anchor
            };

        } 

        if (marker.type == "service_hub") {
            marker1 = new google.maps.Marker({
                // title: title,
                position: pos,
                icon: icon,
                zIndex: i,
                // animation: google.maps.Animation.DROP,
                map: map
            });
        } else {
            if (iconcolor == 'orange') {
                zindexval = 1000;
            } else {
               zindexval = i; 
            }
            // alert("zindexval : "+zindexval);
            marker1 = new google.maps.Marker({
                // title: title,
                position: pos,
                // label: {text: parseInt((Math.random() * 10)).toString(), color: "white"} ,
                icon: icon,
                // zIndex: i,
                zIndex: zindexval, 
                // animation: google.maps.Animation.DROP,
                map: map
            });

        }
        marker1.setOpacity(0);
        //bounds.extend(marker1.position);

        marker1['data'] = marker;

        gmarkers1.push(marker1);

        // Marker click listener

        if ("clinic_single" == marker.type) {
            // 
            //---Always show the detailed clinic view--//
            infowindow.setContent(content);
            infowindow.open(map, marker1);
            
            google.maps.event.addListener(marker1, 'load', (function (marker1, content) {
                return function () {
                    hide_sections_for_clinics();
                    infowindow.setContent(content);
                    infowindow.open(map, marker1);
                    map.panTo(this.getPosition());
                    check_val();
                }
            })(marker1, content));
        }

        if ("clinic" == marker.type) {
            google.maps.event.addListener(marker1, 'click', (function (marker1, content) {
                return function () {
                    hide_sections_for_clinics();
                    delete_markers();
                    load_markers(clinictype, clinicslug, marker1.data.slug);
                    var zoom = 10;
                    if (marker1.data.type == "city") {
                        zoom = 13;
                    }

                    map.panTo(this.getPosition());
                    check_val();

                }
            })(marker1, content));
            
            google.maps.event.addListener(marker1, 'mouseover', (function( content) {
                return function () {
                infowindow.setContent(content);
                infowindow.open(map, this);
            }
              })(content));
            google.maps.event.addListener(marker1, 'mouseout', function() {
                infowindow.close();
            });
        }

        if (marker.type != "service_hub" && marker.type != "clinic" && marker.type != "clinic_single") {
            google.maps.event.addListener(marker1, 'click', (function (marker1, content) {
                return function () {
                    show_sections_not_for_clinics();
                    delete_markers();
                    clinictype = marker1.data.type;
                    clinicslug = marker1.data.slug;
                    load_markers(marker1.data.type, marker1.data.slug);
                    var zoom = 10;
                    if (marker1.data.type == "city") {
                        zoom = 13;
                    }

                    map.panTo(this.getPosition());

                    check_val();

                }
            })(marker1, content));
        }
    }
    setTimeout(function () {
        fadeInMarkers(gmarkers1);
        reset_temp_markers();
    }, 200);    
}

/**
 * Function to add marker to map
 */

function render_markers() {
    closeInfoWindow();
    console.log("TEMP MARKERS", tempmarkers);
    for (i = 0; i < tempmarkers.length; i++) {
        marker = tempmarkers[i];
        var pos = new google.maps.LatLng(marker.lat, marker.lng);
        var content = '<div style="max-width: 240px;text-align: left;padding: 10px 0px 10px 0px"><h4 style="font-size:14px!important;margin:0px;line-height:20px">' + marker.label + '</h4></div>'


        var icon;
        if (marker.type == "clinic" || marker.type == "clinic_single") {
            icon = {
                url: iconBase + 'blue.png', // url
                scaledSize: new google.maps.Size(28, 28), // scaled size
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(28, 28) // anchor
            };

        } else if (marker.marker_type == "service_hub") {
            icon = {
                url: iconBase + 'service_hub.png', // url
                scaledSize: new google.maps.Size(54, 54), // scaled size
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(28, 28) // anchor
            };

        } else if (marker.marker_type == "office") {
            icon = {
                url: iconBase + 'office.png', // url
                scaledSize: new google.maps.Size(54, 54), // scaled size
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(28, 28) // anchor
            };

        } else if (marker.marker_type == "both") {
            icon = {
                url: iconBase + 'both.png', // url
                scaledSize: new google.maps.Size(54, 54), // scaled size
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(28, 28) // anchor
            };

        } else {
            icon = {
                url: icons[marker.type].icon, // url
                scaledSize: new google.maps.Size(54, 54), // scaled size
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(28, 28) // anchor
            };

        } 

        if (marker.type == "service_hub") {
            marker1 = new google.maps.Marker({
                // title: title,
                position: pos,
                icon: icon,
                zIndex: i,
                // animation: google.maps.Animation.DROP,
                map: map
            });
        } else {
            marker1 = new google.maps.Marker({
                // title: title,
                position: pos,
                // label: {text: parseInt((Math.random() * 10)).toString(), color: "white"} ,
                icon: icon,
                zIndex: i,
                // animation: google.maps.Animation.DROP,
                map: map
            });

        }
        marker1.setOpacity(0);
        //bounds.extend(marker1.position);

        marker1['data'] = marker;

        gmarkers1.push(marker1);

        // Marker click listener

        if ("clinic_single" == marker.type) {
            //---Always show the detailed clinic view--//
            infowindow.setContent(content);
            infowindow.open(map, marker1);
            
            google.maps.event.addListener(marker1, 'load', (function (marker1, content) {
                return function () {
                    hide_sections_for_clinics();
                    infowindow.setContent(content);
                    infowindow.open(map, marker1);
                    map.panTo(this.getPosition());
                    check_val();
                }
            })(marker1, content));
        }

        if ("clinic" == marker.type) {
            google.maps.event.addListener(marker1, 'click', (function (marker1, content) {
                return function () {
                    hide_sections_for_clinics();
                    delete_markers();
                    // load_markers(marker1.data.type, marker1.data.slug);
                    load_markers(clinictype, clinicslug, marker1.data.slug);
                    var zoom = 10;
                    if (marker1.data.type == "city") {
                        zoom = 13;
                    }

                    map.panTo(this.getPosition());
                    check_val();

                }
            })(marker1, content));
            
            google.maps.event.addListener(marker1, 'mouseover', (function( content) {
                return function () {
               // infowindow.setContent('<div style="max-width: 240px;text-align: left;padding: 10px 0px 10px 0px"><h4 style="font-size:14px!important;margin:0px;line-height:20px">' + marker1.data.label + '</h4></div>');
                infowindow.setContent(content);
                infowindow.open(map, this);
            }
              })(content));
            google.maps.event.addListener(marker1, 'mouseout', function() {
                infowindow.close();
            });
        }

        if (marker.type != "service_hub" && marker.type != "clinic" && marker.type != "clinic_single") {
            google.maps.event.addListener(marker1, 'click', (function (marker1, content) {
                return function () {
                    show_sections_not_for_clinics();
                    delete_markers();
                    clinictype = marker1.data.type;
                    clinicslug = marker1.data.slug;
                    load_markers(marker1.data.type, marker1.data.slug);
                    var zoom = 10;
                    if (marker1.data.type == "city") {
                        zoom = 13;
                    }
                    map.panTo(this.getPosition());
                    check_val();

                }
            })(marker1, content));
        }
    }
    setTimeout(function () {
        fadeInMarkers(gmarkers1);
        reset_temp_markers();
    }, 200);
}


function filterDeviceCall() {
        filters = [];
        filter_counter = 0;
        var deviceselectedtext = '';
        var devicename, device_id;
        jQuery("input[type='checkbox']:checked").each(function (i) {
            device_id = jQuery(this).val();
            filters.push("device[]=" + device_id);
            filter_counter++;
            var device = $(this).attr("id");
            device = device.split('-');
            var devicefullname = '';
            for (i = 1; i < device.length; i++) {
                devicefullname += device[i] + '-';
            }
            devicefullname = devicefullname + '~@#&@';
            devicefullname = devicefullname.replace("-~@#&@", "");
            devicename = jsUcfirst(devicefullname);
            deviceselectedtext = deviceselectedtext + '' + devicename + ', ';
        });
        if (deviceselectedtext != '') {
            $('#filterview').show();
            deviceselectedtext = deviceselectedtext + '~@#&@';
            deviceselectedtext = deviceselectedtext.replace(", ~@#&@", "");
            $('#devices_selected').text(deviceselectedtext);
        } else {
            $('#devices_selected').text(deviceselectedtext);
            $('#filterview').hide();
        }
}

function hide_sections_for_clinics() {
    jQuery(".card.blue").hide();
    var str1 = $('.mapview-label').text();

    if (str1 == "Middle East") {
        $('[id^="maintab_"]').show();
        $('[id^="device_"]').show();
        if ($('.tab-pane:first').is(':visible') == false ) {
            $('[id^="maintab_"]:first > a').click();
        }
    } else {
        var pieces = str1.split(/[\s,]+/);
        var countryName = $.trim(pieces[pieces.length-1]);
        var ajax_url = "/api/report/deviceByCountry?country=" + countryName;
        $('[id^="maintab_"]').hide();
        $('[id^="device_"]').hide();
        $('[class^="tabpane_"]').hide();
        var existingchecks = [];
        $('[id^="device_"]').find(".form-check-input:checked").each(function () {
            //alert("Id: " + $(this).attr("id") + " Value: " + $(this).val());
            existingchecks.push($(this).attr("id"));
        });
        $('[id^="device_"]').find(".form-check-input"). prop("checked", false);
        jQuery.get(ajax_url, function (response) {
            resjson = response;
            $.each(resjson, function( index, value ) {
                $("#maintab_" + slugify(value.label)).show();
                if (index == '0') {
                    if ($("#maintab_"+slugify(value.label)+" > a.active").length < 1) {
                        $("#maintab_"+slugify(value.label)+" > a").click();
                    }
                    firsttab = slugify(value.label);
                    $.each(value.devices, function( dev_index, dev_value ) {
                      $("#device_" + slugify(dev_value.label)).show();
                    });
                    // $(".tabpane_" + slugify(value.label)).show();
                }
                $.each(value.devices, function( dev_index, dev_value ) {
                  if (jQuery.inArray( slugify(dev_value.label), existingchecks ) != -1) {
                        $("#"+slugify(dev_value.label)).prop("checked", true);                         
                  }
                });
            });
        });
    }
        
    var str2 = "Kuwait";
    if(str1.indexOf(str2) !== -1) {
        $(".clinicbluekuwait").show();
        $(".clinicblue").hide();  
        // For device filter area  
//        $("#maintab_candela").hide();
//        $("#candela").hide();
//        $("#device_16-picoplus").show();
//        $("#device_06-clarity").show();
//        $("#candela").find(".form-check-input"). prop("checked", false);
//        if ($('.tab-pane').is(':visible') == false ) {
//           $("#maintab_lutronics > a").click(); 
//        }
    } else {
        $(".clinicbluekuwait").hide();
        $(".clinicblue").show();
        // For device filter area
//        $("#maintab_candela").show();
//        $("#device_16-picoplus").hide();
//        $("#device_06-clarity").hide();
//        $("#16-picoplus"). prop("checked", false);
//        $("#06-clarity"). prop("checked", false);
    }
    jQuery(".card.light-blue").hide();
    jQuery(".card.blue .card-item:eq(2)").hide();
    jQuery(".card.blue .card-item").removeClass("col-lg-4").addClass("col-lg-6");

    if (jQuery("#market_share span").html() == "0") {
        jQuery("#market_share span").html("-");
        jQuery("#market_share").next("sup").hide();
    }
    if (jQuery("#solutions span").html() == "0") {
        jQuery("#solutions span").html("-");
    }

}

function show_sections_not_for_clinics() {
    jQuery(".card.orange, .card.red, .card.light-blue").show();
    jQuery("#market_share").next("sup").show();
    jQuery(".card.blue .card-item:eq(2)").show();
    jQuery(".card.blue .card-item").removeClass("col-lg-6").addClass("col-lg-4");

}

// Sets the map on all markers in the array.
function setMapOnAll(map) {
    for (var i = 0; i < gmarkers1.length; i++) {
        gmarkers1[i].setMap(map);
    }
}

// Removes the markers from the map, but keeps them in the array.
function clear_markers() {
    setMapOnAll(null);
}

// Shows any markers currently in the array.
function show_markers() {
    setMapOnAll(map);
}

// Deletes all markers in the array by removing references to them.
function delete_markers() {
    clear_markers();
    gmarkers1 = [];
}

google.maps.Map.prototype.panToWithOffset = function (latlng, offsetX, offsetY) {
    var map = this;
    var ov = new google.maps.OverlayView();
    ov.onAdd = function () {
        var proj = this.getProjection();
        var aPoint = proj.fromLatLngToContainerPixel(latlng);
        aPoint.x = aPoint.x + offsetX;
        aPoint.y = aPoint.y + offsetY;
        map.panTo(proj.fromContainerPixelToLatLng(aPoint));
    };
    ov.draw = function () {};
    ov.setMap(this);
};



function fadeInMarkers(markers) {

    if (markerOpacity <= 1) {

        for (var i = 0, len = markers.length; i < len; ++i) {
            markers[i].setOpacity(markerOpacity);
        }

        // increment opacity
        markerOpacity += markerOpacityIncrement;

        // call this method again
        setTimeout(function () {
            fadeInMarkers(markers);
        }, 50);

    } else {
        markerOpacity = markerOpacityIncrement; // reset for next use
    }

}