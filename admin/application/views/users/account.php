<!DOCTYPE html>
<!--<html lang="en">  
<head>
<link href="<?php echo base_url(); ?>assets/css/style.css" rel='stylesheet' type='text/css' />
</head>
<body>
<div class="container">
    <h2>User Account</h2>
    <h3>Welcome <?php echo $user['name']; ?>!</h3>
    <div class="account-info">
        <p><b>Name: </b><?php echo $user['name']; ?></p>
        <p><b>Email: </b><?php echo $user['email']; ?></p>
        <p><b>Phone: </b><?php echo $user['phone']; ?></p>
        <p><b>Gender: </b><?php echo $user['gender']; ?></p>
    </div>
</div>
</body>
</html>-->

<!DOCTYPE html>
<?php 
$this->load->view('header');
?>

        <!-- page content -->
        <div class="right_col" role="main" style="height:1000px!important;">
          <div class="">
                <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                    <h2><b>Clinics and Device Installation</b></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <div class="bs-example" data-example-id="simple-jumbotron">
                    <div class="jumbotron">
<!--                      <h1>Hello, world!</h1>-->
                      <p><?php echo $clinics_and_device_installation; ?></p>
                    </div>
                  </div>

                </div>
              </div>
            </div>
                              <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                    <h2><b>Device Installation Count</b></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <div class="bs-example" data-example-id="simple-jumbotron">
                    <div class="jumbotron">
<!--                      <h1>Hello, world!</h1>-->
                      <p><?php echo $device_installation_count; ?></p>
                    </div>
                  </div>

                </div>
              </div>
            </div>
                              <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                    <h2><b>Import Map Report</b></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <div class="bs-example" data-example-id="simple-jumbotron">
                    <div class="jumbotron">
<!--                      <h1>Hello, world!</h1>-->
                      <p><?php echo $device_count; ?></p>
                    </div>
                  </div>

                </div>
              </div>
            </div>
                              <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                    <h2><b>Import Map Report Marketing</b></h2>

                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <div class="bs-example" data-example-id="simple-jumbotron">
                    <div class="jumbotron">
<!--                      <h1>Hello, world!</h1>-->
                      <p><?php echo $report_marketing_count; ?></p>
                    </div>
                  </div>

                </div>
              </div>
            </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                    <h2><b>Report Social Media</b></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <div class="bs-example" data-example-id="simple-jumbotron">
                    <div class="jumbotron">
<!--                      <h1>Hello, world!</h1>-->
                      <p><?php echo $report_social_media_count; ?></p>
                    </div>
                  </div>

                </div>
              </div>
            </div>   
              
                <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                    <h2><b>Coverage Clinics and Device Installation</b></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <div class="bs-example" data-example-id="simple-jumbotron">
                    <div class="jumbotron">
<!--                      <h1>Hello, world!</h1>-->
                      <p><?php echo $coverage_clinics_and_device_installation; ?></p>
                    </div>
                  </div>

                </div>
              </div>
            </div>
              
        </div>
        </div>
        <!-- /page content -->
<?php 
$this->load->view('footer');
?>
      
