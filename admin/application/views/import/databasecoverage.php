
<!DOCTYPE html>
<?php 
$this->load->view('header');
?>
<script type="text/javascript">

function cleardata(){ 
    var r = confirm("Do you really want to clear data?");
    if (r == true) {
        var url = "<?php  echo base_url() ?>import/database_coverage_clear_data";
        $(location).attr('href',url);
    }       
}

</script>
  
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Coverage Data Import</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <div class="col-md-12">
                        <div class="col-md-3">&nbsp;</div>
<!--                            <div class="col-md-6">
                                <div id="data-count" style="color:grey;">

                                   Import Data from Live Server : <a class="btn btn-primary" href="<?php  echo base_url() ?>import/database_coverage_import_process" style="background-color:#f6b40e;border-color: white">Data Sync</a>
                                </div>
                            </div>-->
                            <form method="post" action="<?php  echo base_url() ?>import/database_coverage_import_process" enctype="multipart/form-data">
<!--                             <label>Select Excel File</label>-->
                             <input type="file" name="excel" />
                             <br />
                             <input type="submit" name="import" class="btn btn-info" value="Import" />
                             <input type="button" name="clear" onClick="cleardata();" class="btn btn-info" value="Clear" />                             
                            </form>
                        </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    
        <!-- /page content -->
<?php 
$this->load->view('footer');
?>
      
