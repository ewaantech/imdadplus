// the smooth zoom function
function smoothZoom (map, max, cnt) {
    if (cnt >= max) {
        return;
    }
    else {
        z = google.maps.event.addListener(map, 'zoom_changed', function(event){
            google.maps.event.removeListener(z);
            smoothZoom(map, max, cnt + 1);
        });
        setTimeout(function(){map.setZoom(cnt)}, 200); // 80ms is what I found to work well on my system -- it might not work well on all systems
    }
}  



function fit_bounds_markers(zoom_level){

    map.fitBounds(bounds);
    //(optional) restore the zoom level after the map is done scaling
    
    var listener = google.maps.event.addListener(map, "idle", function () {
        // map.setZoom(zoom_level);        
        google.maps.event.removeListener(listener);
    });  
    

 smoothZoom(map, zoom_level, map.getZoom());   
    
}


/**
 * Function to load all markers for country or city from the json file
 * @param string type 
 * @param string  slug 
 */
function load_previous_markers( type, slug, update_breadcrumb ){
    delete_markers();
    
   
    switch(type){
        default:
            bounds = new google.maps.LatLngBounds();
            $.each(json.markers,function(i,item){
                add_location_marker(item);
            });            
            break;            
        case 'country':
            bounds      = new google.maps.LatLngBounds();
            $.each(eval("json.countries."+slug+".cities"),function(i,item){
                //console.log("Country", item.markers);
                add_location_marker(item.markers);
            });

            FloatingPopup.populate_popup(eval("json.countries."+slug+".report"));
            load_service_hubs(slug,null);            
            jQuery("#mapview span.mapview-label").html(eval("json.countries." + slug + ".label"));
            //fit_bounds_markers(9);            
            break;
        case 'gcc':   
            bounds = new google.maps.LatLngBounds();  
            FloatingPopup.populate_popup(eval("json.report"));       
            jQuery("#mapview span.mapview-label").html("Middle East");
            break;            
            
    }
    console.log(breadcrumb);
    //console.log(pointers);
}



/**
 * Function to load all markers for country or city from the json file
 * @param string type 
 * @param string  slug 
 */
function load_markers( type, slug, update_breadcrumb ){
    delete_markers();

    var updatebc = true;

    switch(type){
        default:
            bounds = new google.maps.LatLngBounds();
            $.each(json.markers,function(i,item){
                add_location_marker(item);
            });            
            break;            
        case 'country':
            bounds      = new google.maps.LatLngBounds();            
            breadcrumb.push(slug);                        
            $.each(eval("json.countries."+slug+".cities"),function(i,item){
                // console.log("Country", item.markers);
                add_location_marker(item.markers);
            });
            FloatingPopup.populate_popup(eval("json.countries."+slug+".report"));
            load_service_hubs(slug,null);            
            jQuery("#mapview span.mapview-label").html(eval("json.countries." + slug + ".label"));
            break;
        case 'city':   

            breadcrumb.push(slug);
            bounds = new google.maps.LatLngBounds();  
            FloatingPopup.populate_popup(eval("json.countries."+breadcrumb[1]+".cities."+slug+".report"));       
            load_service_hubs(breadcrumb[1], slug);   
            jQuery("#mapview span.mapview-label").html(eval("json.countries." + breadcrumb[1] + ".cities."+ slug + ".label"));
            break;            
            
    }

    console.log("Updated Breadcurmbs", breadcrumb);
}


function load_service_hubs(country, city){
    if( country != null && country != "undefined" && country !="" ){
        if( city != null && city != "undefined" && city !="" ){
            $.each(eval("json.service_hubs." + country + "." + city),function(i,item){
                add_location_marker(item);
            });

        }else{
            $.each(eval("json.service_hubs." + country),function(i,item){
                $.each(item, function(i,childitem){
                    add_location_marker(childitem);
                });                
            });
        }
    }    
}


/**
 * Function to add marker to map
 */

function add_location_marker(marker) {
    var title = marker.label;
    // console.log(marker.lat, marker.lng);
    var pos = new google.maps.LatLng(marker.lat, marker.lng);
    var content = marker.sku;
    
    var icon;

    if( marker.type == "service_hub"){
        icon = {
            url: iconBase + marker.icon, // url
            scaledSize: new google.maps.Size(40, 40), // scaled size
            origin: new google.maps.Point(0,0), // origin
            anchor: new google.maps.Point(30, 30) // anchor
        };
            
    }else{
        icon = {
            url: icons[marker.type].icon, // url
            scaledSize: new google.maps.Size(54, 54), // scaled size
            origin: new google.maps.Point(0,0), // origin
            anchor: new google.maps.Point(30, 30) // anchor
        };
            
    }

    if( marker.type == "service_hub"){
        marker1 = new google.maps.Marker({
            title: title,
            position: pos,
            icon: icon,
            animation: google.maps.Animation.DROP,
            map: map
        });
    }else{
        marker1 = new google.maps.Marker({
            title: title,
            position: pos,
            // label: {text: parseInt((Math.random() * 10)).toString(), color: "white"} ,
            icon: icon,
            animation: google.maps.Animation.DROP,
            map: map
        });
        
    }
    bounds.extend(marker1.position);
    
    marker1['data'] = marker;

    //console.log(marker1.slug);

    gmarkers1.push(marker1);

    //console.log(gmarkers1);
    // Marker click listener

    if( marker.type != "service_hub"){
        google.maps.event.addListener(marker1, 'click', (function (marker1, content) {
            return function () {

                delete_markers();
                load_markers(marker1.data.type, marker1.data.slug);
                var zoom = 9;
                if( marker1.data.type == "city"){
                    zoom = 11;
                }
                // map.setZoom(zoom)
                // map.setZoom(parseInt(marker1.data.zoom));
                // console.log(this.sku);
                // infowindow.setContent(content);
                // infowindow.open(map, marker1);
                //map.setCenter(new google.maps.LatLng(mylat,mylong));
                //map.fitBounds(bounds);
                //(optional) restore the zoom level after the map is done scaling
                //marker1.setMap(map);
                //map.fitBounds(bounds);
                map.panTo(this.getPosition());
                

                fit_bounds_markers(zoom);
                
                        
                
            }
        })(marker1, content));
    }

    
}



// Sets the map on all markers in the array.
function setMapOnAll(map) {
    for (var i = 0; i < gmarkers1.length; i++) {
        gmarkers1[i].setMap(map);
    }
}

// Removes the markers from the map, but keeps them in the array.
function clear_markers() {
    setMapOnAll(null);
}

// Shows any markers currently in the array.
function show_markers() {
    setMapOnAll(map);
}

// Deletes all markers in the array by removing references to them.
function delete_markers() {
    clear_markers();
    gmarkers1 = [];
}

google.maps.Map.prototype.panToWithOffset = function(latlng, offsetX, offsetY) {
    var map = this;
    var ov = new google.maps.OverlayView();
    ov.onAdd = function() {
        var proj = this.getProjection();
        var aPoint = proj.fromLatLngToContainerPixel(latlng);
        aPoint.x = aPoint.x+offsetX;
        aPoint.y = aPoint.y+offsetY;
        map.panTo(proj.fromContainerPixelToLatLng(aPoint));
    }; 
    ov.draw = function() {}; 
    ov.setMap(this); 
};