<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * User Management class created by CodexWorld
 */
class Users extends MY_Controller {
    
    function __construct() {
        $this->data = parent::__construct();
//        echo "<pre>";
//            print_r($data);
//            exit;
        $this->load->library('form_validation');
    }
    
    /*
     * User account information
     */
    public function account(){
        if($this->data['tablecount'] == 1){
            redirect('import/database_sync');
        }
//        $data = array();
//        $data['user'] = $this->user->getRows(array('id'=>$this->session->userdata('userId')));
        //load the view
        $clinicDeviceInstallCount = 0;
        $clinicCount = 0;
        $mapReportCount = 0;
        $deviceCount = 0;
        $reportMarketingCount = 0;
        $reportSocialMediaCount = 0;

        $this->data['coverage_clinics_and_device_installation'] =  "No Records Found";
        $this->data['clinics_and_device_installation'] =  "No Records Found";
        $this->data['device_installation_count'] =  "No Records Found";
        $this->data['device_count'] =  "No Records Found";
        $this->data['report_marketing_count'] =  "No Records Found";
        $this->data['report_social_media_count'] =  "No Records Found";
     
        
        
        $clinicDeviceInstallCount = $this->Me_model->getClinicDeviceInstallCount();
        $clinicCount = $this->Me_model->getClinicCount();
        if($clinicDeviceInstallCount > 0){
         $this->data['clinics_and_device_installation'] = "Records : <b>$clinicCount - Clinics</b> and <b>$clinicDeviceInstallCount - Devices</b>.";
        }        
        
        $clinicDeviceInstallCount = $this->Me_model->getCoverageClinicDeviceInstallCount();
        $clinicCount = $this->Me_model->getCoverageClinicCount();
        if($clinicDeviceInstallCount > 0){
         $this->data['coverage_clinics_and_device_installation'] = "Records : <b>$clinicCount - Clinics</b> and <b>$clinicDeviceInstallCount - Devices</b>.";
        }
        
         $mapReportCount = $this->Me_model->getMapReportCount();
         if($mapReportCount > 0){
            $this->data['device_count'] = "Records : <b>$mapReportCount - Map Report(s)</b>.";
         }
         
        $deviceCount= $this->Me_model->getDeviceCount(); 
        if($deviceCount > 0){
            $this->data['device_installation_count'] = "Records : <b>$deviceCount - Device Installation Count</b>.";
        }
         
        $reportMarketingCount= $this->Me_model->getReportMarketingCount();
        if($reportMarketingCount > 0){
            $this->data['report_marketing_count'] = "Records : <b>$reportMarketingCount - Map Report Marketing</b>.";
        }
 
        $reportSocialMediaCount= $this->Me_model->getReportSocialMediaCount();
        if($reportSocialMediaCount > 0){
            $this->data['report_social_media_count'] = "Records : <b>$reportSocialMediaCount - Report Social Media</b>.";
        }        
        $this->load->view('users/account', $this->data);
    }
    
  
    /*
     * User logout
     */
    public function logout(){
        $this->session->unset_userdata('isUserLoggedIn');
        $this->session->unset_userdata('userId');
        $this->session->sess_destroy();
        // redirect('users/login/');
        redirect(base_url(''));
    }
    
    /*
     * User logout
     */
    public function profile(){
//        $data = array();
//        $data['user'] = $this->user->getRows(array('id'=>$this->session->userdata('userId')));
        if($this->input->post('profileUpdate')){
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('user-name', 'User Name', 'required');
            if ($this->form_validation->run() == true) {

                $userData = array(
                    'id' => $this->data['user']['id'], 
                    'name' => strip_tags($this->input->post('user-name')),
                    'email' => strip_tags($this->input->post('email'))
                );
                $this->user->profileUpdate($userData);
                $this->session->set_flashdata('message','Successfully updated the profile');
                redirect(base_url('users/profile'));

            } else {
                $data['user'] = array(
                    'name' => strip_tags($this->input->post('user-name')),
                    'email' => strip_tags($this->input->post('email'))
                ); 
                //load the view
                $this->load->view('users/profile', $data);           
            }
        } else {
            //load the view
            $this->load->view('users/profile', $this->data);           
        }
    }
    
    /*
     * To change the password
     */
    public function changepassword(){
        if($this->input->post('changePassword')){
            $this->form_validation->set_rules('pass-word', 'Password', 'required');
            if ($this->form_validation->run() == true) {
                $this->user->changePassword($this->data['user']['id'], $this->input->post('pass-word'));
                $this->session->set_flashdata('message','Successfully updated the password');
                redirect(base_url('users/changepassword'));
            }
        }
        //load the view
        $this->load->view('users/changepassword', $this->data);        
    }
    
    /*
     * Existing email check during validation
     */
    public function email_check($str){
        $con['returnType'] = 'count';
        $con['conditions'] = array('email'=>$str);
        $checkEmail = $this->user->getRows($con);
        if($checkEmail > 0){
            $this->form_validation->set_message('email_check', 'The given email already exists.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

}