<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cities extends CI_Model{
    /*
     * get rows from the users table
     */
    function cityList($params = array()){
        $data = array();
        $this->db->select('tbl_city.City_Name as cityname, tbl_city.City_Id as cityid, tbl_country.Country_Name as countryname, tbl_city.City_Zoom as zoom, nearest.City_Name as nearest_city_name');
        $this->db->join('tbl_country', 'tbl_country.Country_Id = tbl_city.Country_Id', 'left');
        $this->db->join('tbl_city as nearest', 'nearest.City_Id = tbl_city.Nearest_city_id', 'left');
        $result = $this->db->get('tbl_city');
        $data = $result->result_array();
        return $data; 
        
    }
    
    
    function countryList($params = array()){
        $data = array();
        $this->db->select('tbl_country.Country_Id as countryid, tbl_country.Country_Name as countryname');
        $result = $this->db->get('tbl_country');
        $data = $result->result_array();
        return $data; 
        
    }
    
        function insertCity($cityResults){
            $basedata = array( 
                'Country_Id' => $cityResults['details']['Country_Id'],
                'City_Name' => $cityResults['details']['City_Name'],
                'City_Latitude' => $cityResults['details']['City_Latitude'],  
                'City_Longitude' => $cityResults['details']['City_Longitude'],
                //'City_Zoom' => 11,
                'city_centre_lat' => $cityResults['details']['city_centre_lat'],
                'city_centre_long'=>  $cityResults['details']['city_centre_long'],
                'City_Zoom'=>  $cityResults['details']['zoom'],
                'sitecore_lat' => $cityResults['details']['sitecore_lat'],
                'sitecore_lng'=>  $cityResults['details']['sitecore_lng'],
                'sitecore_zoom'=>  $cityResults['details']['sitecore_zoom'],
                'marker_type'=> $cityResults['details']['marker_type'],
                'Nearest_city_id' => $cityResults['details']['nearest_city'],
                'density_point_1_lat'=> $cityResults['details']['density_point_1_lat'],
                'density_point_1_lng'=> $cityResults['details']['density_point_1_lng'],
                'density_point_2_lat'=> $cityResults['details']['density_point_2_lat'],
                'density_point_2_lng'=> $cityResults['details']['density_point_2_lng'],
                'density_point_3_lat'=> $cityResults['details']['density_point_3_lat'],
                'density_point_3_lng'=> $cityResults['details']['density_point_3_lng'],
                'set_high_density'=> "YES",
                'set_medium_density'=> "YES",
                'set_low_density'=> "YES"
             );

            $this->db->insert('tbl_city', $basedata);//Insert to parent table

        
        }
        
        function getCity($id){
            $data = array();
            $this->db->select('*');
            $this->db->where('City_Id', $id);
            $result = $this->db->get('tbl_city');
            $data = $result->result_array();
            return $data; 
        
    }
    
    
    function updateCity($cityResults, $id){

            $basedata = array( 
                'Country_Id' => $cityResults['details']['Country_Id'],
                'City_Name' => $cityResults['details']['City_Name'],
                'City_Latitude' => $cityResults['details']['City_Latitude'],  
                'City_Longitude' => $cityResults['details']['City_Longitude'],
               // 'City_Zoom' => 11,
                'City_Zoom'=>  $cityResults['details']['zoom'],
                'city_centre_lat' => $cityResults['details']['city_centre_lat'],
                'city_centre_long'=>  $cityResults['details']['city_centre_long'],
                'sitecore_lat' => $cityResults['details']['sitecore_lat'],
                'sitecore_lng'=>  $cityResults['details']['sitecore_lng'],
                'sitecore_zoom'=>  $cityResults['details']['sitecore_zoom'],
                'marker_type'=>  $cityResults['details']['marker_type'],
                'Nearest_city_id' => $cityResults['details']['nearest_city'],
                'density_point_1_lat'=> $cityResults['details']['density_point_1_lat'],
                'density_point_1_lng'=> $cityResults['details']['density_point_1_lng'],
                'density_point_2_lat'=> $cityResults['details']['density_point_2_lat'],
                'density_point_2_lng'=> $cityResults['details']['density_point_2_lng'],
                'density_point_3_lat'=> $cityResults['details']['density_point_3_lat'],
                'density_point_3_lng'=> $cityResults['details']['density_point_3_lng'],
                'set_high_density'=> $cityResults['details']['set_high_density'],
                'set_medium_density'=> $cityResults['details']['set_medium_density'],
                'set_low_density'=> $cityResults['details']['set_low_density']                
             );
        //echo '<pre>'; print_r($basedata); exit;
        $this->db->where('City_id', $id);
        
        $this->db->update('tbl_city', $basedata); 
    }
    
    function deleteCity($city_id){

        $this->db->where('City_Id', $city_id);
        $this->db->delete('tbl_city');
        
    }
    
            function getCountry($id){
            $data = array();
            $this->db->select('*');
            $this->db->where('Country_Id', $id);
            $result = $this->db->get('tbl_country');
            $data = $result->result_array();
            return $data; 
        
    }
}