<?php

/**
 * Created by PhpStorm.
 * User: ben
 * Date: 3/13/18
 * Time: 9:03 AM
 */
class Country_model extends CI_Model {

    /**
     * Get country marker data
     *
     * @param $filters
     * @return array
     * @throws Exception
     */
    public function getMarkerData($data) {

        $where = array();

        $country_id = isset($data['country_id']) ? (int) $data['country_id'] : 0;
        $city_id = isset($data['city_id']) ? (int) $data['city_id'] : 0;

        // can be array fo devices
        $device_id = isset($data['devices_id']) ? $data['devices_id'] : 0;

        if ($country_id > 0) {
            $where[] = 'tc.Country_Id = ' . $country_id;
        }

        if ($city_id) {
            $where[] = 'tc.City_Id = ' . $city_id;
        }

        if (is_array($device_id) && count($device_id) > 0) {
            $where[] = 'tdi.Device_Id IN (' . implode(',', $device_id) . ')';
        } elseif ($device_id > 0) {
            $where[] = 'tdi.Device_Id = ' . $device_id;
        }

        $where = implode(' AND ', $where);
        if ($where) {
            $where = "WHERE $where";
        }

        $sql = "SELECT DISTINCT c.Country_Id, c.Country_Latitude, c.Country_Longitude, c.Country_Name, c.Country_Zoom, c.country_centre_lat,
		c.country_centre_long FROM tbl_country AS c INNER JOIN tbl_clinics AS tc ON tc.Country_Id = c.Country_Id INNER JOIN tbl_device_install AS tdi
		ON tdi.Clinic_Id = tc.Clinic_Id $where";

//		echo $sql."<br>";

        $query = $this->db->query($sql);

        $result = $query->result();


        $_country = array();

        if (count($result)) {
            foreach ($result as $country) {
                $_country[slugify($country->Country_Name)] = array(
                    'id' => $country->Country_Id,
                    'lat' => $country->Country_Latitude,
                    'lng' => $country->Country_Longitude,
                    'label' => $country->Country_Name,
                    'slug' => slugify($country->Country_Name),
                    'type' => 'country',
                    'zoom' => $country->Country_Zoom,
                    'center_lat' => $country->country_centre_lat,
                    'center_lng' => $country->country_centre_long
                );
            }
        }

        return $_country;
    }

    /**
     * Get all country data based on filter
     *
     * @param $filters
     * @return array
     * @throws Exception
     */
    public function getAll($filters = array()) {

        $where = array();

        // can be array fo devices
        $device_id = isset($filters['devices_id']) ? $filters['devices_id'] : 0;

        if (is_array($device_id) && count($device_id) > 0) {
            $where[] = 'tdi.Device_Id IN (' . implode(',', $device_id) . ')';
        } elseif ($device_id > 0) {
            $where[] = 'tdi.Device_Id = ' . $device_id;
        }

        $where = implode(' AND ', $where);
        if ($where) {
            $where = "WHERE $where";
        }

        $sql = "SELECT DISTINCT c.Country_Id, c.Country_Latitude, c.Country_Longitude, c.Country_Name, c.Country_Zoom, c.country_centre_lat,
		c.country_centre_long FROM tbl_country AS c INNER JOIN tbl_clinics AS tc ON tc.Country_Id = c.Country_Id INNER JOIN tbl_device_install AS tdi
		ON tdi.Clinic_Id = tc.Clinic_Id $where";

//		echo $sql."<br>";

        $query = $this->db->query($sql);

        return $query->result();



        /**
          if( isset($filters['devices_id']) && $filters['devices_id'] > 0 ){
          $this->db->join('tbl_map_report', 'tbl_map_report.Country_Id = tbl_country.Country_Id');

          if( is_array( $filters['devices_id']  ) && count( $filters['devices_id']  ) > 0 ){
          $this->db->where_in('tbl_map_report.Device_Id', $filters['devices_id'] );
          }
          elseif( $filters['devices_id']  > 0 ){
          $this->db->where('tbl_map_report.Device_Id', $filters['devices_id'] );
          }

          }


          $query = $this->db->get('tbl_country');

          return $query->result();
         */
    }
    
    /**
     * Get latitude and longitude of a city or country
     *
     * @param $data
     * @return array
     * @throws Exception
     */
    public function getLatLng($data) {
        $sql = '';
        $details = array();
        if (isset($data['city_name']) && isset($data['country_name'])) {
            $sql = "SELECT DISTINCT c.City_Id as id, c.Nearest_city_id as Nearest_city_id,c.Country_Id as Country_Id,c.City_Latitude as lat, c.City_Longitude as lng, c.City_Name as name, c.sitecore_zoom as zoom, c.sitecore_lat as center_lat,
		c.sitecore_lng as center_lng FROM tbl_city AS c INNER JOIN tbl_country AS tc ON tc.Country_Id = c.Country_Id where c.City_Name = '".$data['city_name']."' and tc.Country_Name = '".$data['country_name']."'";
        } else if (isset($data['country_name'])) {
            $sql = "SELECT DISTINCT c.Country_Id as id, c.Country_Latitude as lat, c.Country_Longitude as lng, c.Country_Name as name, c.sitecore_zoom as zoom, c.sitecore_lat as center_lat,
		c.sitecore_lng as center_lng FROM tbl_country AS c where c.Country_Name = '".$data['country_name']."'" ;
        }
        
        if ($sql == '') {
            return $details;
        }

        $query = $this->db->query($sql);

        $result = $query->result();


        if (count($result)) {
            foreach ($result as $det) {
                $details = array(
                    'id' => $det->id,
                    'lat' => $det->lat,
                    'lng' => $det->lng,
                    'label' => $det->name,
                    'slug' => slugify($det->name),
                    'zoom' => $det->zoom,
                    'center_lat' => $det->center_lat,
                    'center_lng' => $det->center_lng
                );
                if (isset($data['city_name']) && isset($data['country_name'])) {
                    $details['Nearest_city_id'] = $det->Nearest_city_id;
                    $details['Country_Id'] = $det->Country_Id;
                }
            }
        }

        return $details;
    }

        function getCountryId($country_name){
          
            $data = array();
            $this->db->select('Country_Id');
            $this->db->where('Country_Name', $country_name);
            $result = $this->db->get('tbl_country');
            $data = $result->result_array();
            
           if(count($data) == 0){
               echo 'Country name supplied is not matching';
               exit;
           } else {
               return $data[0]['Country_Id'];
           }
                     
    } 
}
